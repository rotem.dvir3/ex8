package com.example.ex0.postpc.ex8;

import junit.framework.TestCase;

import org.junit.Assert;
import org.junit.Test;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.InstrumentationRegistry;
import androidx.work.Configuration;
import androidx.work.impl.utils.SynchronousExecutor;

import com.example.ex0.postpc.ex8.MyWorkMangerPackage.MyWorkManager;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.eq;
@RunWith(RobolectricTestRunner.class)
@Config(sdk = {Build.VERSION_CODES.O_MR1})

public class TodoItemsHolderImplTest {
    private ActivityController<MainActivity> activityController;
    private DataHolder mockDataBase;

//    @Before
//    public void setup(){
//        mockDataBase = Mockito.mock(DataHolder.class);
//        // when asking the `mockDataBase` to get the current items, return an empty list
//        Mockito.when(mockDataBase.getCurrentItems())
//                .thenReturn(new ArrayList<>());
//
//        activityController = Robolectric.buildActivity(MainActivity.class);
//
//        // let the activity use our `mockDataBase` as the TodoItemsDataBase
//        MainActivity activityUnderTest = activityController.get();
//        activityUnderTest.holder = mockDataBase;




//        // Initialize WorkManager for instrumentation tests.
//
//    }


    @Test

    public void when_addingTodoItem_then_callingListShouldHaveThisItem(){
        // setup
        Context context = InstrumentationRegistry.getTargetContext();

        DataHolder holderUnderTest = new DataHolder(context);
        Assert.assertEquals(0, holderUnderTest.getCurrentItems().size());

        // test
        holderUnderTest.addNewItem(new NumRoots(7,7,1,UUID.randomUUID()));

        // verify
        Assert.assertEquals(1, holderUnderTest.getCurrentItems().size());
    }

    @Test
    public void when_deletingTodoItem_then_callingListShouldnotHaveThisItem_1(){
        // setup
        Context context = InstrumentationRegistry.getTargetContext();

        DataHolder holderUnderTest = new DataHolder(context);
        Assert.assertEquals(0, holderUnderTest.getCurrentItems().size());

        // test
        holderUnderTest.addNewItem(new NumRoots(7,7,1,UUID.randomUUID()));
        holderUnderTest.deleteItem(holderUnderTest.cur_items_proccessing.get(0));

        // verify
        Assert.assertEquals(0, holderUnderTest.getCurrentItems().size());
    }

    @Test
    public void when_marking_item_done_4(){
        // setup
        Context context = InstrumentationRegistry.getTargetContext();

        DataHolder holderUnderTest = new DataHolder(context);
        holderUnderTest.addNewItem(new NumRoots(7,7,1,UUID.randomUUID()));
        holderUnderTest.addNewItem(new NumRoots(6,3,2,UUID.randomUUID()));

        holderUnderTest.addNewItem(new NumRoots(1,1,1,UUID.randomUUID()));

    



        // verify
        Assert.assertEquals(3, holderUnderTest.getCurrentItems().size());
    }



}
