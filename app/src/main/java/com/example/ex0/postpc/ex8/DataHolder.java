package com.example.ex0.postpc.ex8;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Parcelable;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class DataHolder {
    ArrayList<NumRoots> cur_items_proccessing;
    ArrayList<NumRoots> cur_items_done;

    private static Context myContext =null;
    private static SharedPreferences pref;
    private static final MutableLiveData<List<NumRoots>> myMutable = new MutableLiveData<>();
    public static final LiveData<List<NumRoots>> myLiveDate=myMutable;

    @RequiresApi(api = Build.VERSION_CODES.O)
    protected DataHolder(Context context){
        cur_items_proccessing =new ArrayList<>();
        cur_items_done = new ArrayList<>();

        myContext=context;
        pref = context.getSharedPreferences("local_db_app",Context.MODE_PRIVATE);
        set_mutable();

    }



    @RequiresApi(api = Build.VERSION_CODES.O)
    private void set_mutable(){
        for(String name:pref.getAll().keySet()){
            add_item(new NumRoots(pref.getString(name,null)));}
        Collections.sort(cur_items_proccessing);
        Collections.sort(cur_items_done);

        myMutable.setValue(getCurrentItems());




    }

    private void add_item(NumRoots item){
        if(item.Done){
            cur_items_done.add(item);
            return;
        }
        cur_items_proccessing.add(item);



    }

    public NumRoots getItem(UUID curnum){
        for (NumRoots todo:cur_items_proccessing) {
            if (todo.ID.equals(curnum)){
                return todo;

            }

        }
        for (NumRoots todo:cur_items_done) {
            if (todo.ID.equals(curnum)){
                return todo;

            }

        }

        return null;
    }

    public LiveData<List<NumRoots>> getLiveData() {
        return myLiveDate;
    }

    public void change_item_roots(UUID id,long root1,long root2) {
        NumRoots item = getItem(id);
        item.progress=100;
        item.root1=root1;
        item.root2=root2;
        SharedPreferences.Editor editor = pref.edit();
        editor.remove(String.valueOf(item.ID));
        editor.putString(String.valueOf(item.ID),item.toString());
        editor.apply();
        myMutable.setValue(getCurrentItems());



    }

    public void change_item_last(UUID id,long last,double pr) {
        NumRoots item = getItem(id);
        item.progress=pr;
        item.last=last;
        SharedPreferences.Editor editor = pref.edit();
        editor.remove(String.valueOf(item.ID));
        editor.putString(String.valueOf(item.ID),item.toString());
        editor.apply();
            myMutable.setValue(getCurrentItems());



    }


    public ArrayList<NumRoots> getCurrentItems() {

        ArrayList<NumRoots> newList = new ArrayList<>(cur_items_proccessing);
        newList.addAll(cur_items_done);
        return newList; }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void addNewItem(NumRoots num) {
         boolean inlist =false;
        for(NumRoots a :cur_items_done){
            if (num.ID==a.ID){
                cur_items_done.remove(a);
                cur_items_proccessing.add(a);
                inlist=true;

                break;
            }
        }
        if (!inlist){
            cur_items_proccessing.add(num);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(String.valueOf(num.ID),num.toString());
            editor.apply();
            myMutable.setValue(getCurrentItems());
        }

        Collections.sort(cur_items_proccessing);




    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void markItemDone(NumRoots item) {
        if(!item.Done){
            cur_items_proccessing.remove(item);
            cur_items_done.add(item);
            item.Done=true;
            Collections.sort(cur_items_done);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(String.valueOf(item.ID),item.toString());
            editor.apply();
            myMutable.setValue(getCurrentItems());

        }

    }
//    public void sendBroadCastDBchanges(){
//        Intent brod = new Intent("db_changed");
//        brod.putExtra("new_list", (Parcelable) this.getCurrentItems());
//        myContext.sendBroadcast(brod);
//    }



    public void deleteItem(NumRoots item) {
        if (item.Done){
            cur_items_done.remove(item);
        }
        else{
            cur_items_proccessing.remove(item);

        }

        SharedPreferences.Editor editor = pref.edit();
        editor.remove(String.valueOf(item.ID));
        editor.apply();
        myMutable.setValue(getCurrentItems());

    }



}


