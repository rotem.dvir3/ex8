package com.example.ex0.postpc.ex8;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

public class NumRoots implements  Comparable<NumRoots> , Serializable {
    public long num;
    public long root1;
    public long root2;
    public long last;
    public UUID ID;
    public double progress;
    public boolean Done =false;
    LocalDateTime crationDate;



    @RequiresApi(api = Build.VERSION_CODES.O)
    public NumRoots(long n, long r1, long r2, UUID id ){
        num=n;
        root1=r1;
        root2=r2;
        ID=id;
        crationDate =java.time.LocalDateTime.now() ;
        last=2;
        progress=2;



    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    public NumRoots(String des){
        String[] param =des.split("\\$");
        num=Long.parseLong(param[1]);
        ID=UUID.fromString(param[0]);
        Done = Boolean.parseBoolean(param[2]);
        root1 = Long.parseLong(param[3]);
        root2 = Long.parseLong(param[4]);
        crationDate = LocalDateTime.parse(param[5]);
        last = Long.parseLong(param[6]);
        progress=Double.parseDouble(param[7]);


    }

    public void markItemDone(){
        Done=true;
    }

    public void markItemUnDone(){
        Done=false;
    }

    public String getDescription(){

        return "Num:  "+num;
    }

    public String getRoot1des(){
        if (root1==1 || root2==1){
            return "Num is prime!!!!!";
        }

        return "Root 1 : "+root1;
    }

    public String getRoot2des(){
        if (root1==1 || root2==1){
            return "";
        }

        return "Root 2 : "+root2;
    }

    @Override
    public String toString(){


        String newString = ID +
                "$" +
                this.num +
                "$" +
                this.Done +
                "$" +
                this.root1 +
                "$" +
                this.root2 +
                "$" +
                this.crationDate+"$"+last+"$"+progress;
        return newString;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public int compareTo(NumRoots o) {
        LocalDateTime compareQuantity = o.crationDate;
        if (this.crationDate.isBefore(compareQuantity)){
            return 1;
        }
        //ascending order
        return -1;
    }

    String getStatusdes(){
        if (Done){
            return "Status: Done!";
        }
        return "Status: in Progress";
    }


}
