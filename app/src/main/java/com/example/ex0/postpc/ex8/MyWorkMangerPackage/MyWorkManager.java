package com.example.ex0.postpc.ex8.MyWorkMangerPackage;

import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.example.ex0.postpc.ex8.NumRoots;
import com.example.ex0.postpc.ex8.SingeltonApp;

import java.util.UUID;

public class MyWorkManager extends Worker {


    public MyWorkManager(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }



    @NonNull
    @Override
    public Result doWork() {
        long num = getInputData().getLong("num",0);
        long root1 = num;
        long root2= 1;
        long j = SingeltonApp.getInstance().getDatabase().getItem(this.getId()).last;
        for(long i =j; i<num;i++){
            if(i*i>num){
                break;
            }
            if (num%i==0){
                root1=i;
                root2=num/i;
                break;

            }
            double p = 1000 * i / (double)num;
            if(p<100000){
                 p = 100 * i / (double)num;


            }

            if(i>10000 && (i%10000==0)){

                setProgressAsync(new Data.Builder().putDouble("prog",p).putLong("last",i).build());

            }


            Log.d("num ","for num :"+num+ " p: "+p);


        }

        return Result.success(new Data.Builder().putLong("root1",root1).putLong("root2",root2).build());
    }


}
