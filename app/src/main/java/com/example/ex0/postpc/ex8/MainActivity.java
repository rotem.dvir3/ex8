package com.example.ex0.postpc.ex8;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.example.ex0.postpc.ex8.MyWorkMangerPackage.MyWorkManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import androidx.lifecycle.Observer;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;
import androidx.work.WorkRequest;


public class MainActivity extends AppCompatActivity {
    public DataHolder holder = null;
    public LinearLayoutManager layoutManager = null;
    public WorkManager myWorker;
    AppAdapter cur_adapter;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (holder == null) {
            // remove and make static
            holder = SingeltonApp.getInstance().getDatabase();
        }
        if (myWorker==null){
            myWorker= WorkManager.getInstance(this);
        }
        if (cur_adapter==null){
            cur_adapter = SingeltonApp.getInstance().getAdapter();
        }
        SingeltonApp.getInstance().setWorkManger(myWorker);
        FloatingActionButton createNewTask = findViewById(R.id.buttonCreateTodoItem);
        EditText taskName = findViewById(R.id.editTextInsertTask);
        RecyclerView recyclerList = findViewById(R.id.recyclerTodoItemsList);
        recyclerList.setAdapter(cur_adapter);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerList.setLayoutManager(layoutManager);
        taskName.setText(""); // cleanup text in edit-text
        taskName.setEnabled(true); // set edit-text as enabled (user can input text)
        createNewTask.setEnabled(false);
//        loadjobs();

        holder.getLiveData().observe(this, new Observer<List<NumRoots>>() {



            @Override
            public void onChanged(List<NumRoots> todoItems) {
                {
                    cur_adapter.notifyDataSetChanged();

                }
            }


        });
        taskName.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void afterTextChanged(Editable s) {

                if (android.text.TextUtils.isDigitsOnly(taskName.getText().toString())){

                    createNewTask.setEnabled(true);

                }
            }
        });



        createNewTask.setOnClickListener(v -> {
                    if (taskName.getText().toString().length() != 0) {
                        long num = Long.parseLong(taskName.getText().toString());
                        OneTimeWorkRequest.Builder new_req= new OneTimeWorkRequest.Builder(MyWorkManager.class);


                        new_req.addTag("findRoots");
                        Data.Builder data = new Data.Builder();
                        data.putLong("num", num);
                        new_req.setInputData(data.build());
                        OneTimeWorkRequest req= new_req.build();
                        myWorker.enqueue(req);
                        NumRoots cur = new NumRoots(num,num,1, req.getId());
                        holder.addNewItem(cur);




                        // make the rcycler show this first
                        cur_adapter.notifyDataSetChanged();
                        taskName.setText("");
                        taskName.setEnabled(true);

                    }
                }
        );
        LiveData<List<WorkInfo>> liveData1 = myWorker.getWorkInfosByTagLiveData("findRoots");


        liveData1.observe(this, new Observer<List<WorkInfo>>() {
            @Override
            public void onChanged(List<WorkInfo> workInfos) {

//
                for(WorkInfo inf: workInfos){
                    NumRoots cur_item = holder.getItem(inf.getId());
                    if (cur_item==null){
                        continue;
                    }

                    if(inf.getState()==WorkInfo.State.SUCCEEDED){
                        Data cur_data= inf.getOutputData();
                        holder.change_item_roots(inf.getId(), cur_data.getLong("root1",0),cur_data.getLong("root2",0));
                        holder.markItemDone(cur_item);

                    }
                    else if (inf.getState()==WorkInfo.State.RUNNING){
                        Data cur_data= inf.getProgress();
                        holder.change_item_last(inf.getId(),cur_data.getLong("last",0),cur_data.getDouble("prog",0));
                    }
                    cur_adapter.notifyDataSetChanged();
                }


            }
        });



    }

//    @RequiresApi(api = Build.VERSION_CODES.O)
//    private void loadjobs() {
//        LiveData<List<WorkInfo>> liveData1 = myWorker.getWorkInfosByTagLiveData("findRoots");
//        List<WorkInfo> info_lst= liveData1.getValue();
//        if (info_lst==null){
//            return;
//        }
//        for (WorkInfo inf: Objects.requireNonNull(info_lst)){
//            if (inf.getState()==WorkInfo.State.RUNNING){
//                NumRoots cur_item = holder.getItem(inf.getId());
//
//                Data.Builder data = new Data.Builder();
//                data.putLong("i",cur_item.last);
//                myWorker.
//
//
//
//
//
//            }
//
//
//        }
//
//        cur_adapter.notifyDataSetChanged();
//
//
//    }





    @Override
    protected void onDestroy() {
        super.onDestroy();


    }

//    @Override
//    protected void onSaveInstanceState(@NonNull Bundle outState) {
//        super.onSaveInstanceState(outState);
//        EditText editTextUserInput = findViewById(R.id.editTextInsertTask);
//        String s = editTextUserInput.getText().toString();
//        outState.putString("user_last_input", s);
////        outState.putParcelable("layer_man", layoutManager.onSaveInstanceState());
//        outState.putSerializable("holder", (Serializable) this.holder);
//
//
//    }
//
//    @Override
//    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
//        super.onRestoreInstanceState(savedInstanceState);
//        EditText editTextUserInput = findViewById(R.id.editTextInsertTask);
//        editTextUserInput.setText(savedInstanceState.getString("user_last_input"));
////        Parcelable state = savedInstanceState.getParcelable("layer_man");
//        holder = (DataHolder) savedInstanceState.getSerializable("holder");
////        layoutManager.onRestoreInstanceState(state);
//
//
//    }


}