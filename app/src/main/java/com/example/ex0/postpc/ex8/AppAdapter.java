package com.example.ex0.postpc.ex8;


import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.provider.DocumentsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;
import java.util.List;


public class AppAdapter extends RecyclerView.Adapter<AppAdapter.AdapterViewHolder> {

    private DataHolder localDataSet;

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    static class AdapterViewHolder extends RecyclerView.ViewHolder {
        TextView description;
        TextView root1;
        TextView root2;
        Button deleteButton;
        TextView Status;
        View mainview;
        ProgressBar pbar;

        public AdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            deleteButton = itemView.findViewById(R.id.delete);
            description = itemView.findViewById(R.id.des);
            root1= itemView.findViewById(R.id.root1);
            root2= itemView.findViewById(R.id.root2);
            Status = itemView.findViewById(R.id.Status);
            mainview=itemView;
            pbar= itemView.findViewById(R.id.determinateBar);
        }
    }


    /**
     * Initialize the dataset of the Adapter.
     *
     * @param curTasks א containing the data to populate views to be used
     *                 by RecyclerView.
     */
    public AppAdapter(DataHolder curTasks) {

        localDataSet = curTasks;
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public AdapterViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.single_row, viewGroup, false);

        return new AdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterViewHolder holder, int position) {
        NumRoots cur_item = localDataSet.getCurrentItems().get(position);
        holder.description.setText(cur_item.getDescription());
        holder.Status.setText(cur_item.getStatusdes());
        holder.root1.setText("Got until: "+cur_item.last);
        holder.root2.setText("...");
        holder.mainview.setBackgroundColor(Color.BLUE);
        holder.pbar.setProgress((int)cur_item.progress);


        if (cur_item.Done){
            holder.deleteButton.setBackgroundResource(R.drawable.ic_menu_delete);
            holder.root1.setText(cur_item.getRoot1des());
            holder.root2.setText(cur_item.getRoot2des());
            holder.mainview.setBackgroundColor(Color.GREEN);
        }




        holder.deleteButton.setOnClickListener(v ->

                {
                    SingeltonApp.getInstance().getMyworker().cancelWorkById(cur_item.ID);
                    this.localDataSet.deleteItem(cur_item);

                    this.notifyDataSetChanged();

                }
        );


    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return localDataSet.getCurrentItems().size();
    }


}




