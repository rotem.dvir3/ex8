package com.example.ex0.postpc.ex8;

import android.app.Application;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.work.Configuration;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;
import androidx.work.WorkRequest;
import androidx.work.Worker;

import com.example.ex0.postpc.ex8.MyWorkMangerPackage.MyWorkManager;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executor;

public class SingeltonApp extends Application {
    public DataHolder getDatabase() {
        return database;
    }

    public WorkManager getMyworker() {
        return myworker;
    }



    public static SingeltonApp getInstance() {
        return instance;
    }
    public AppAdapter getAdapter(){return cur_adapter;}


    private static SingeltonApp instance = null;

    private static DataHolder database;
    private WorkManager myworker ;
    private AppAdapter cur_adapter;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onCreate() {
        super.onCreate();
        if (database == null) {
            database = new DataHolder(this);


        }

        cur_adapter = new AppAdapter(database);




        instance = this;
    }
    public void setWorkManger(WorkManager work){
        myworker=work;
    }


}

